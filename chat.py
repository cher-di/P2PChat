import logging
import socket
import sys
import threading
import time
from configparser import ConfigParser


class Chat:

    @staticmethod
    def __get_ip():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            # doesn't even have to be reachable
            s.connect(('10.255.255.255', 1))
            ip = s.getsockname()[0]
        except:
            ip = '127.0.0.1'
        finally:
            s.close()
        return ip

    def __init__(self):
        pass

